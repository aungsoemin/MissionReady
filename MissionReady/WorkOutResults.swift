//
//  WorkOutResults.swift
//  MissionReady
//
//  Created by Daud on 1/5/17.
//  Copyright © 2017 Nex. All rights reserved.
//

import UIKit

class WorkOutResults: UIViewController {

    //ViewController------------------------------
    var vc : ViewController!
    
    //UIElements---------------------
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnHide: UIButton!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var lblEndurance: UILabel!
    
    var lblResults = [UILabel(),UILabel(),UILabel()]
    
    //content width and height----------------------
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentWidth: NSLayoutConstraint!
    @IBOutlet weak var resultHeight: NSLayoutConstraint!
    
    //variables-------------------
    var headers = ["OVERALL","THIS WEEK","LAST SCORE"]
    
    // C H A N G E   T H E S E   V A R I A B L E S ----------------
    var results = ["4m","0m","103%"]
    var endurance = 3
    
    var resultsHidden = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        profilePic.layer.cornerRadius = profilePic.frame.width / 2
        profilePic.clipsToBounds = true
        profilePic.contentMode = .scaleAspectFill
        setProfilePic()
        
        contentWidth.constant = self.view.frame.width
        
        btnHide.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        for var i in (0 ..< 3)
        {
            let lblHeader = UILabel(frame: CGRect(x: 0, y: 5, width: containerView.frame.width / 3, height: containerView.frame.height / 2))
            lblHeader.text = headers[i]
            lblHeader.textColor = UIColor.white
            lblHeader.font = UIFont(name: "Lato-Regular", size: 15)
            lblHeader.center.x = containerView.frame.width / 3 * CGFloat(i) + containerView.frame.width / 6
            lblHeader.textAlignment = .center
            containerView.addSubview(lblHeader)
            
            let lblResult = UILabel(frame: CGRect(x: 0, y: lblHeader.frame.origin.y + 25, width: containerView.frame.width / 3, height: containerView.frame.height / 2 - 5))
//            lblResult.text = results[i]
            lblResult.textColor = UIColor.white
            lblResult.font = UIFont(name: "Oswald-Regular", size: 16)
            lblResult.center.x = containerView.frame.width / 3 * CGFloat(i) + containerView.frame.width / 6
            lblResult.textAlignment = .center
            lblResults[i] = lblResult
            containerView.addSubview(lblResult)
        }
        
        for var i in (0 ..< 2)
        {
            let seperator = UIView(frame: CGRect(x: 0, y: 0, width: 2, height: containerView.frame.height * 0.6))
            seperator.center.x = containerView.frame.width / 3 * (CGFloat(i) + 1)
            seperator.center.y = containerView.frame.height / 2
            seperator.backgroundColor = UIColor.white
            containerView.addSubview(seperator)
        }
        
        refresh()
        
    }
    
    @IBAction func hideResults(_ sender: UIButton) {
        
        if (!resultsHidden)
        {
            resultsHidden = true
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.btnHide.transform = CGAffineTransform(rotationAngle: 180 * 3.14 / 180)
                self.resultHeight.constant = 40
                self.view.layoutIfNeeded()
                
            }, completion: {finish in
            })
        }
        else
        {
            resultsHidden = false
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.btnHide.transform = CGAffineTransform(rotationAngle: 0 * 3.14 / 180)
                self.resultHeight.constant = 420
                self.view.layoutIfNeeded()
                
            }, completion: {finish in
            })
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: UIButton) {
        vc.back(preView: vc.workoutScreen.view, curView: self.view)
    }
    
    func setProfilePic()
    {
        let userDefault = UserDefaults.standard
        if (userDefault.data(forKey: "profilePic") == nil)
        {
            profilePic.image = UIImage(named: "Avater.png")
        }
        else
        {
            profilePic.image = UIImage(data: userDefault.data(forKey: "profilePic")!)
        }
    }
    
    
    //-------------------REFRESH THE VALUES--------------------------
    //Call this method if you want to refresh the values-------------
    func refresh()
    {
        for var i in (0 ..< 3)
        {
            lblResults[i].text = results[i]
        }
        lblEndurance.text = "Endurance + \(endurance)%"
    }
    

}
