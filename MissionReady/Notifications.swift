//
//  Notifications.swift
//  MissionReady
//
//  Created by Daud on 1/2/17.
//  Copyright © 2017 Nex. All rights reserved.
//

import UIKit

class Notifications: UIViewController {
    
    var vc : ViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: UIButton) {
        
        vc.back(preView: vc.dashboard.view, curView: self.view)
        
    }

}
