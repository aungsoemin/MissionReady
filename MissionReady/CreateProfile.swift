//
//  CreateProfile.swift
//  MissionReady
//
//  Created by Daud on 12/26/16.
//  Copyright © 2016 Nex. All rights reserved.
//

import UIKit

class CreateProfile: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //ViewControllers------------------------
    var vc : ViewController!
    
    //UIElements----------------------------
    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtSecondName: UITextField!
    @IBOutlet weak var rowView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    
    let btnSex = UIButton()
    let txtWeight = UITextField()
    let btnWeight = UIButton()
    
    //Content height and width for scrollView
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentWidth: NSLayoutConstraint!
    
    //Variables---------------------------
    var keyboardShowed = false
    var curProfilePic = UIImage() // current choosen profile pic
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //UI Adjustments------------------------------
        
        contentWidth.constant = self.view.frame.width
        if (self.view.frame.height <= 568)
        {
            contentHeight.constant = 568
        }
        else
        {
            contentHeight.constant = self.view.frame.height
        }
        txtFirstName.layer.cornerRadius = 2
        txtSecondName.layer.cornerRadius = 2
        
        view.layoutIfNeeded() // Updating frame changes after constraint change
        
        btnSex.frame = CGRect(x: 0, y: 0, width: (rowView.frame.width / 2) - 10, height: rowView.frame.height)
        btnSex.backgroundColor = UIColor(red: 67/255, green: 68/255, blue: 78/255, alpha: 1)
        btnSex.setTitle("Sex", for: .normal)
        btnSex.layer.cornerRadius = 2
        
        let container = UIView()
        container.layer.cornerRadius = 2
        container.backgroundColor = UIColor.clear
        container.frame = CGRect(x: (rowView.frame.width / 2) + 10, y: 0, width: (rowView.frame.width / 2) - 10, height: rowView.frame.height)
        container.layer.cornerRadius = 2
        container.clipsToBounds = true
        
        txtWeight.frame = CGRect(x: 0, y: 0, width: (container.frame.width) - container.frame.height, height: container.frame.height)
        txtWeight.backgroundColor = UIColor(red: 67/255, green: 68/255, blue: 78/255, alpha: 1)
        txtWeight.text = "   Weight"
        txtWeight.backgroundColor = UIColor(red: 67/255, green: 68/255, blue: 78/255, alpha: 1)
        container.addSubview(txtWeight)
        
        btnWeight.frame = CGRect(x: container.frame.width - container.frame.height, y: 0, width: container.frame.height, height: container.frame.height)
        btnWeight.backgroundColor = UIColor(red: 67/255, green: 68/255, blue: 78/255, alpha: 1)
        btnWeight.setTitle("KG", for: .normal)
        container.addSubview(btnWeight)
        
        rowView.addSubview(container)
        rowView.addSubview(btnSex)
        rowView.backgroundColor = UIColor.clear
        
        self.view.backgroundColor = UIColor.clear
        profilePic.layer.cornerRadius = profilePic.frame.width / 2
        profilePic.clipsToBounds = true
        profilePic.contentMode = .scaleAspectFill
        setProfilePic()
        
        
        //Tap to hide keyboard
        self.scrView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.keyboardHide(_:))))
        self.scrView.isUserInteractionEnabled = true
        
        //Keyboard notification to listen to keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //to get the keyboard height--------------------
    func keyboardWillShow(notification: NSNotification)
    {
        if (!keyboardShowed && vc.curView == "createProfile")
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
                //            let keyboardHeight = keyboardSize.height
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
                    
                    self.view.frame.origin.y = -(self.txtFirstName.frame.origin.y - 20) + 50
//                    self.view.layoutIfNeeded()
                    
                }, completion: nil)
                keyboardShowed = true
                
            }
        }
    }
    
    //Hide Keyboard----------------------------
    func keyboardHide(_ gesture : UITapGestureRecognizer)
    {
        if (keyboardShowed)
        {
            self.view.endEditing(true)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
                
                self.view.frame.origin.y = 0
                self.view.layoutIfNeeded()
                
            }, completion: nil)
            keyboardShowed = false
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        
        keyboardHide(UITapGestureRecognizer())
        vc.back(preView: vc.signInEmail.view, curView: self.view)
        vc.curView = "signInEmail"
        
    }
    
    @IBAction func `continue`(_ sender: UIButton) {
        
        keyboardHide(UITapGestureRecognizer())
        vc.selectMission.view.frame = CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        (vc.selectMission as! SelectMission).vc = self.vc
        vc.view.addSubview(vc.selectMission.view)
        
        
        vc.goNext(curView: self.view, nextView: vc.selectMission.view)
        
    }
    
    //Change Profile Pic----------------------------------------
    
    func setProfilePic()
    {
        let userDefault = UserDefaults.standard
        if (userDefault.data(forKey: "profilePic") == nil)
        {
            profilePic.image = UIImage(named: "Avater.png")
        }
        else
        {
            profilePic.image = UIImage(data: userDefault.data(forKey: "profilePic")!)
        }
    }
    
    @IBAction func changeProfilePic(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        vc.present(picker, animated : true, completion : nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            let userDefault = UserDefaults.standard
            userDefault.set(UIImageJPEGRepresentation(pickedImage, 1) as NSData?, forKey: "profilePic")
            profilePic.image = pickedImage
        }
        vc.dismiss(animated:true, completion: nil)
    }
    
    

}
