//
//  AdjustInput.swift
//  MissionReady
//
//  Created by Daud on 1/5/17.
//  Copyright © 2017 Nex. All rights reserved.
//

import UIKit

class AdjustInput: UIViewController {

    //ViewControllers---------------------------
    var vc : ViewController!
    
    //UI Elements------------------------
    @IBOutlet weak var lblTime: UILabel!
    
    //Variables-------------------------
    var curHeartRate = 0
    var curIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
    }
    
    func setTime(rate : Int, curIndex : Int)
    {
        self.curIndex = curIndex
        curHeartRate = rate
        lblTime.text = "\(curHeartRate)"
    }
    
    @IBAction func increase(_ sender: UIButton) {
        if (curHeartRate < 150)
        {
            curHeartRate = curHeartRate + 1
            lblTime.text = "\(curHeartRate)"
        }
    }
    
    @IBAction func decrease(_ sender: UIButton) {
        if (curHeartRate > 50)
        {
            curHeartRate = curHeartRate - 1
            lblTime.text = "\(curHeartRate)"
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        (vc.workoutScreen as! WorkoutScreen).dismissPopUp(UITapGestureRecognizer())
    }
    
    @IBAction func set(_ sender: UIButton) {
        (vc.workoutScreen as! WorkoutScreen).curHeartRates[curIndex] = curHeartRate
        (vc.workoutScreen as! WorkoutScreen).refreshHeartRate()
        (vc.workoutScreen as! WorkoutScreen).dismissPopUp(UITapGestureRecognizer())
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
