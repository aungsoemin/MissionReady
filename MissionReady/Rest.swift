//
//  Rest.swift
//  MissionReady
//
//  Created by Daud on 1/5/17.
//  Copyright © 2017 Nex. All rights reserved.
//

import UIKit

class Rest: UIViewController {
    
    //ViewControllers---------------------------
    var vc : ViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func rest(_ sender: UIButton) {
        (vc.workoutScreen as! WorkoutScreen).curPopUp = 3
        (vc.workoutScreen as! WorkoutScreen).dismissPopUp(UITapGestureRecognizer())
        
    }

}
