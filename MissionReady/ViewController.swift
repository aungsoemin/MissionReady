//
//  ViewController.swift
//  MissionReady
//
//  Created by Daud on 12/23/16.
//  Copyright © 2016 Nex. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    //View Controllers------------------
    
    var signIn : UIViewController!
    var signInEmail : UIViewController!
    var createProfile : UIViewController!
    var selectMission : UIViewController!
    var dashboard : UIViewController!
    var settings : UIViewController!
    var notifications : UIViewController!
    var editProfile : UIViewController!
    var workoutOverview : UIViewController!
    var workoutScreen : UIViewController!
    var adjustInput : UIViewController!
    var adjustTime : UIViewController!
    var rest : UIViewController!
    var workOutResults : UIViewController!
    
    //UI Elements------------------
    @IBOutlet weak var imgView: UIImageView!
    
    //Variables----------------------
    var curView = "" // Tells the class of current view-----------------
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ViewControllers Initialize----------------------------
        
        signIn = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "SignIn") as! SignIn
        signInEmail = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "SignInEmail") as! SignInEmail
        createProfile = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "createProfile") as! CreateProfile
        selectMission = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "selectMission") as! SelectMission
        dashboard = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "dashboard") as! Dashboard
        settings = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "settings") as! Settings
        notifications = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "notifications") as! Notifications
        editProfile = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "editProfile") as! EditProfile
        workoutOverview = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "workoutOverview") as! WorkoutOverview
        workoutScreen = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "workoutScreen") as! WorkoutScreen
        adjustInput = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "adjustInput") as! AdjustInput
        adjustTime = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "adjustTime") as! AdjustTime
        rest = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "rest") as! Rest
        workOutResults = UIStoryboard(name: "Main", bundle : nil).instantiateViewController(withIdentifier: "workOutResults") as! WorkOutResults
        
        (signIn as! SignIn).vc = self
        self.view.addSubview(signIn.view)
        
        
        //Tap to hide keyboard
        self.imgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.keyboardHide(_:))))
        self.imgView.isUserInteractionEnabled = true
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Common Functions-----------------------------
    
    func goNext(curView : UIView, nextView : UIView)
    {
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            nextView.frame.origin.x = 0
            curView.frame.origin.x = -self.view.frame.width
            
        }, completion: {finish in
        })
    }
    
    func back(preView : UIView, curView : UIView)
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            preView.frame.origin.x = 0
            curView.frame.origin.x = self.view.frame.width
            
        }, completion: {finish in
            curView.removeFromSuperview()
        })
    }
    
    func keyboardHide(_ gesture : UITapGestureRecognizer)
    {
//        print("hello")
        if (curView == "signInEmail")
        {
            (signInEmail as! SignInEmail).keyboardHide(UITapGestureRecognizer())
        }
        else if (curView == "createProfile")
        {
            (createProfile as! CreateProfile).keyboardHide(UITapGestureRecognizer())
        }
        else if (curView == "editProfile")
        {
            (editProfile as! EditProfile).keyboardHide(UITapGestureRecognizer())
        }
    }


}

